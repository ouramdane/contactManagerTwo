package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import entities.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
