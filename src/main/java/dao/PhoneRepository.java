package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import entities.Phone;

public interface PhoneRepository extends JpaRepository<Phone, Long>{

}
