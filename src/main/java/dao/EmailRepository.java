package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import entities.Email;

public interface EmailRepository extends JpaRepository<Email, Long>{

}
