package web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import dao.PersonRepository;
import entities.Person;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

public class PersonController {
	
	@Autowired
	private PersonRepository personRepo;

	@RequestMapping(value="/api/contacts",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Person>> getPersons() {
		List<Person> listPerson = personRepo.findAll();
		return ResponseEntity.ok().body(listPerson);

	}
}
