package Seeders;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import com.github.javafaker.Faker;

import dao.AddressRepository;
import dao.EmailRepository;
import dao.PersonRepository;
import dao.PhoneRepository;
import entities.Person;

@Component
public class DataBaseSeeder {

	private EmailRepository emailRepo;
	private PersonRepository personRepo;
	private AddressRepository addRepo;
	private PhoneRepository phoneRepo;

	Faker faker = new Faker();

	@Autowired
	public DataBaseSeeder(EmailRepository emailRepo, PersonRepository personRepo, AddressRepository addRepo,
			PhoneRepository phoneRepo) {
		super();
		this.emailRepo = emailRepo;
		this.personRepo = personRepo;
		this.addRepo = addRepo;
		this.phoneRepo = phoneRepo;
	}

	@EventListener
	public void seed(ContextRefreshedEvent event) {

		//emailRepo.deleteAll();
		personRepo.deleteAll();
		seedPersonTable();
	

	}

	// ******************SEED Persons*************//
	private void seedPersonTable() {

	    for (int i = 0; i < 10; i++) {
	    	String firstName = faker.name().firstName(); // Emory
	    	String lastName = faker.name().lastName(); // Barton
	    	Person per = new Person(firstName, lastName);
	   		personRepo.save(per);	
	    }
	    System.out.println("**********Person table seeded********");
	    System.out.println("le nombre de produits est: " + personRepo.count());
	}
	

}
