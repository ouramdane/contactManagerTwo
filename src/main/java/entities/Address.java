package entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.transaction.Transactional;

import lombok.Data;


@Data
@Transactional
@Entity
public class Address implements Serializable{
	@Id @GeneratedValue
	private Long addressId;
	private String street;
	private String extension;
	private String city;
	private String postCode;
	private String country;
    
	@ManyToMany(mappedBy="address")
	private Collection <Person> personAddresses;
	
	
	
	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(String street, String extension, String city, String postCode, String country) {
		super();
		this.street = street;
		this.extension = extension;
		this.city = city;
		this.postCode = postCode;
		this.country = country;
	}

	
}
