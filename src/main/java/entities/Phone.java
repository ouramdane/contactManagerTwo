package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.transaction.Transactional;
import lombok.Data;



@Data
@Entity
@Transactional

public class Phone implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long phoneId;
	private String phoneNumber;
	
	public Phone() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Phone(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
	}
	
}
